package com.SL4JJSON;

import com.google.gson.Gson;

public class Person {
	private int id;
	private String name;
	
	
	
	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}



	public Person(int id, String name) {
		super();
		this.id = id;
		this.name = name;
	}



	public int getId() {
		return id;
	}



	public void setId(int id) {
		this.id = id;
	}



	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}
	
	@Override 
	public String toString() { 
	    return new Gson().toJson(this);
	}

}
